# BioTyperConversion


## Purpose

This repository provides a R script that allows conversion of Bruker Biotyper HTML output into a Microsoft Excel table.

## Install the repository

```shell
cd <parent_directory_for repos>
git clone https://gitlab.com/Brendy/biotyperconversion.git
```

## Details

The `parse_biotyper.R` script converts the Bruker BioTyper HTML Report into an Excel spreadsheet with a BioTyper Score Distance Matrix.

The **requirements** are:
* install the programming language "R"
* install the visual environment "RStudio"
* the following R packages will be installed on the first run (if not present): "tidyverse", "lubridate", "xml2", "rvest", "gtools", "xlsx"
* place your report in the `input` subfolder of this repo
* make sure it has the filename and file extension "Bruker Daltonik MALDI Biotyper Classification Results.htm"


Proceed with opening the script `parse_biotyper.R` in RStudio.
* edit line 32 and repace `workingDirectory <- file.path("<path_to_repo>")` with the path of you installation.

If you want to generate a matrix of more than 10 by 10 (the BioTyper report default output of scores for each sample), increase the number of results per sample. We tested this script with 62 by 62 samples.

Hit the Button "source" in the upperleft corner or press CTRL+SHIFT+ENTER.
Read the notes of the console window while executing the script and find the results in the "output" subfolder.
Please do NOT save anything while closing RStudio.

## Author and Acknowledgment

* Dr. Holger Brendebach <Holger. Brendebach (at) bfr. bund. de>

## License
For open source projects, say how it is licensed.

## Project status
running version, beta status, last tested with BioTyper report in late 2019
